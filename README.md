Controller:
- Raspberry Pi B
- Microchip MCP3008 ADC


Sensors:
- water level sensor : feedback for automatic refill
- temperature sensor : aquarium temperature

Future:
- Alexa/AWS (swap to Raspi3)